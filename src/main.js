import Vue from 'vue'
import VueRouter from "vue-router"
import Vuex from 'vuex'
import 'es6-promise/auto'
import App from './App.vue'
import MainAuthentication from "./MainAuthentication.vue";
import NavigationMap from "./NavigationMap.vue";
import * as VueGoogleMaps from "vue2-google-maps";
import axios from 'axios';
import {GEOCODE_URI,MAP_KEY} from './utils/Constants';

Vue.config.productionTip = false;

/* Including the Navigation Router */
Vue.use(VueRouter);
Vue.use(Vuex);

/* Including the google map module */
Vue.use(VueGoogleMaps, {
    load: {
        key: MAP_KEY,
        libraries: "places" // necessary for places input
    }
});


const fakeUsers = [
    {id:1,username:"demo1",password:'demo1',token:'AZERTY'},
    {id:2,username:"demo2",password:'demo2',token:'QWERTY'},
];

/* Store */
// eslint-disable-next-line
const store = new Vuex.Store({
    state: {

        /* Session token for backend auth */
        token: localStorage.getItem('user-token'),
        isAuthenticating : false,

        currentLocation: null,
        markers: [],

        /* Géolocation state , required for the loading message dialog */
        geolocationLoading : false

    },
    mutations: {

        /* Temporary selection of a location on the map */
        setCurrentLocation(state, payload) {
            state.currentLocation = {
                lat: payload.location.lat,
                lng: payload.location.lng,
            };
        },

        /* Persist the markers in localstorage */
        persistCurrentLocation(state,{markers}){

            state.markers = markers;
            state.currentLocation = null;

            localStorage.setItem("favorite_markers", JSON.stringify(state.markers));

        },

        /* Clear the current location to hide the marker dialog */
        clearCurrentLocation(state) {
            state.currentLocation = null;
        },

        /* Auth */
        logOut(state){
            state.isAuthenticating = false;
            state.token = '';
            localStorage.removeItem('user-token');
        },

        login(state,{token}){

            state.isAuthenticating = true;
            state.token = token;

            localStorage.setItem('user-token',token);

        },

        loginError(state){

            state.isAuthenticating = false;
            state.token = '';

        },
        /* Auth */

        deleteMarker(state,{index}){

            let markers = [...state.markers];
            markers.splice(index, 1);

            localStorage.setItem("favorite_markers", JSON.stringify(markers));
            state.markers = markers;

        }

    },

    actions:{

        /* @todo refactor to external service */
        addCurrentLocationToFavoriteMarkers ({ commit, state }, payload) {

            if (state.currentLocation !== null) {

                let latitude = state.currentLocation.lat;
                let longitude = state.currentLocation.lng;

                /* change geolocation state to show the loading message */
                state.geolocationLoading = true;

                /* Request Reverse geocode to get the location name */
                axios.get(`${GEOCODE_URI}?key=${MAP_KEY}&latlng=${latitude},${longitude}`)
                    .then(response => {

                        if (response.data.results.length !== 0) {

                            /* Make a copy of the existing markers */
                            let markers = [...state.markers];

                            /* Extraction location full address */
                            let address = response.data.results[0].formatted_address;

                            /* add the new location to the collection */
                            markers.push({
                                name : payload.name,
                                lat: state.currentLocation.lat,
                                lng: state.currentLocation.lng,
                                comment: payload.comment,
                                address: address
                            });

                            /* Invoke the mutator */
                            commit('persistCurrentLocation',{markers})

                        }
                    })
                    .catch(function (error) {
                        // handle error
                        alert('The is an issue with the geocode api');
                    })
                    .finally(function () {
                        state.geolocationLoading = false;
                    });

            }


        },


        /* Load markers from the localstorage on request */
        fetchLocalStorageMarkers({state}){

            /* retrieve the markers from localstorage */
            let storedMarkers = localStorage.getItem("favorite_markers");

            /* check markers presence if true overwrite the markers state */
            if(storedMarkers!==null){
                state.markers = JSON.parse(storedMarkers);
            }

        },

        /* Authentication Request */
        authRequest({commit},payload){

            return new Promise((resolve, reject) => {

                if( payload.username ==='' || payload.password ==='' ){
                    reject();
                }

                /* check the user presence */
                let user = fakeUsers.find((user)=>{
                    return ( user.username === payload.username && user.password === payload.password );
                });

                if(user){
                    commit('login',{token:user.token});
                    resolve();
                } else {
                    commit('loginError');
                    reject();
                }

            });


        },

    }

});

/* trigger the initial localstorage fetch */
store.dispatch('fetchLocalStorageMarkers');

const routes = [
    {path: '/', name: 'login',component: MainAuthentication},
    {path: '/navigation-map',name: 'map', component: NavigationMap}
];

const router = new VueRouter({routes});

new Vue({
    render: h => h(App),
    router: router,
    store,
    components: {App}

}).$mount('#app');